##  Passion.io Front-end Challenge ##

Hi!
To run this program you need to have Node (>=8) and NPM installed on your local machine.
1. Install all the dependencies

`npm install`

2. Run the program in development mode


`npm run dev`

3. Open your browser to start the application
          	 `http://localhost:3000`

**Stack used in this project:**

- React
- Redux
- Next.js (for SSR and fast prototyping)
- Built-in CSS in JS solution
- SCSS
- ES6 and some ES7
- Node
- Express
- Flexbox

----------


##How does your solution perform?
Monitoring the performance in the Google Chrome console I concluded that even in development mode the application performs well, the most time spent is with scripting and even so far I have obtained good results.
##How does your solution scale?
The application is prepared to receive new categories in all fields because I am monitoring the state of the application with Redux and the UI is displaying the results with separate components. From the standpoint of styles there are three possible solutions that enable scalability. In my opinion, using the global style sheet and maintaining specific styles within each component helps with both scalability and the integration and documentation of each component.
##What would you improve next?
I would improve the visual part, but I can not think of improvements because I do not have so much artistic appeal.
It would also improve the animations to make the interaction smoother.
From the application architecture point of view, I would further break down the components and separate some responsibilities.
##What was one of the biggest coding challenges that you ever had?
The biggest difficulty I had was when I was developing a user assistance platform where there were many forms with similar fields, but each one behaved differently.
I started to realize that I was repeating a lot of code and the number only increased, I had to think about how to solve it because every time there was a change to be made (and there was always) it took me a long time to change these codes.
I thought of an architecture with High Order Components that helped solve much of the problem, but ultimately I decided to work with render props that re-solved the problem that the HOCs left as a conflict of props names, etc.