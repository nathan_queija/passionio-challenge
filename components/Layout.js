import React, { Component } from 'react';
import stylesheet from 'styles/index.scss';
import Head from 'next/head';



class Layout extends Component{

    constructor(props){
        super(props);

    }

    render(){
        return(
            <div>
                {<style dangerouslySetInnerHTML={{ __html: stylesheet }} />}
                <Head>
                    <title>{ this.props.title || 'Passion.io' }</title>
                </Head>


                    <nav className="navbar navbar-main" role="navigation">
                        <div className="container">
                            <div className="navbar-header">
                                <div className="navbar-brand">
                                    <div className="header-logo">
                                        <img src="/static/logo.svg" alt="Passion logo" />
                                    </div>
                                </div>
                            </div>



                        </div>
                    </nav>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Layout;