import React, { Component } from 'react';

class Category extends Component{


    getContent(content){
        const {onChoose} = this.props;
       return content.map(actualContent => {
           return(
               <div key={actualContent.Id} className="item" onClick={() => onChoose(actualContent)}>
                   <p>{actualContent.Name}</p>
                   {actualContent.Author && <span>by {actualContent.Author}</span>}
               </div>
           );
       })
    }


    render(){

        const {message, name, content} = this.props;
        return(
            <div>
                {name && <h2 className={`${name.toLowerCase()} category-title`}>{name}</h2>}
                {message && <p>{message}</p>}
                <div className="category-container">
                    {this.getContent(content)}
                </div>
            </div>
        )
    }
}

export default Category;