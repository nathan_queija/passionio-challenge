import React from 'react';
import { connect } from 'react-redux';
import { concatContent, findMatches } from 'utils';


class SearchFilter extends React.Component {

    constructor(props){
        super(props);

        const {verticals, courses, categories} = props;

        this.state = {
            content: concatContent([
                {name: "Vertical", content: verticals.all},
                {name: "Course", content: courses.all},
                {name: "Category", content: categories.all}
            ]),
            matches: []
        }
    }

    onChangeSearch(event){
        const {value} = event.target;

        if(value.length > 0){
            const matches = findMatches(value, this.state.content)
            this.setState({matches});
        }else{
            this.setState({matches: []});
        }
    }

    displayMatches(){
        const { matches } = this.state;
        return(
            <ul className="suggestions">
                {matches.map(({name, content}) =>  {
                    return(
                        <li key={name+content.Id}>
                            <span className="name">{`${content.Name}`}</span>
                            <span className={`category ${name === 'Category' ? 'categories' : ( name === 'Vertical' ? 'verticals' : 'courses')}`}>{name}</span>

                        </li>
                    )
                })}
            </ul>
        )
    }



    render() {
        return (
            <div>
                <style jsx>
                    {`
                    .field {
                      display:flex;
                      position:relative;
                      margin: auto;
                      width:70%;
                      flex-direction:row;
                    }

                    .field>input[type=text] {
                      flex:1;
                      padding:0.6em;
                      border: none;
                      border-bottom:0.2em solid rgb(197, 31, 0);
                      background-color: #191A20;
                       color: white;
                       font-weight: bold;
                    }
                `}
                </style>
                <div className="field" id="searchform">
                    <input type="text" id="searchterm" placeholder="Start typing to search verticals, categories or courses..." onChange={this.onChangeSearch.bind(this)} />
                </div>
                {this.displayMatches()}
            </div>
        );
    }
}


const mapStateToProps = ({ verticals, categories, courses }) => ({
    verticals,
    categories,
    courses
});
export default connect(mapStateToProps, null)(SearchFilter);
