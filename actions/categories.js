
import { SET_CATEGORY, SET_VERTICAL, SET_COURSE } from './';

export function setCategory(category) {
    return async dispatch => {
        dispatch({ type: SET_CATEGORY, payload: category });
    };
}

export function setVertical(vertical) {
    return async dispatch => {
        dispatch({ type: SET_VERTICAL, payload: vertical });
        dispatch({ type: SET_CATEGORY, payload: {} });
    };
}

export function setCourse(course) {
    return async dispatch => {
        dispatch({ type: SET_COURSE, payload: course });
    };
}