export function concatContent(arrays) {
    const finalArray = [];

    arrays.forEach(element => {
        const actualContentArray = element.content.map(actualContent => ({name: element.name, content: actualContent}));
        finalArray.push(...actualContentArray);
    });

    return finalArray;
}


export function findMatches(word, array){
    return array.filter(actual => {
        const regex = new RegExp(word, 'gi');
        return actual.content.Name.match(regex);
    });
}