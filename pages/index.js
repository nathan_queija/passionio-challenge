import React from 'react';
import withRedux from 'next-redux-wrapper';
import initStore from '../store';
import Layout from 'components/Layout';
import SearchFilter from 'components/SearchFilter';
import Category from 'components/Category';
import { setCategory, setCourse, setVertical } from 'actions/categories';



class Index extends React.Component {

    constructor(props){
        super(props);
    }


    render() {
        const {verticals, categories, courses, setCategory, setCourse, setVertical} = this.props;
        const categoriesFiltered = categories.all.filter(category => category.Verticals === verticals.selected.Id);
        const coursesFiltered = courses.all.filter(course => course.Categories === categories.selected.Id);

        return (
          <Layout>
              <style jsx>
                  {`
                    h1, h2 {
                        text-align: center;
                    }

                    h5.fancy {
                    width:80%;
                    text-align:center;
                    border-bottom: 1px solid #000;
                    line-height:0.1em;
                    margin: auto;
                    margin-top: 40px;

                    }
                    h5.fancy span { background:#fff; padding:0 10px; }
                  `}
              </style>
              <h1>Frontend Coding Challenge</h1>
              <h2>Passionate Navigation</h2>
              <SearchFilter/>
              <h5 className="fancy"><span>or click on the verticals below to navigate </span></h5>
              <Category name="Verticals" message="Select a Vertical:" content={verticals.all} onChoose={setVertical} />
              {verticals.selected.Id && <Category name="Categories" message="Select a Category:" content={categoriesFiltered} onChoose={setCategory}/>}
              {categories.selected.Id && <Category name="Courses"  content={coursesFiltered} onChoose={setCourse}/>}

          </Layout>
        );
    }
}


const mapStateToProps = ({ verticals, categories, courses }) => ({
        verticals,
        categories,
        courses
    });
export default withRedux(initStore, mapStateToProps, {setCategory, setVertical, setCourse})(Index);
