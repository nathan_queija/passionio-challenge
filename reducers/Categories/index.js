import { SET_CATEGORY } from 'actions';

const categories = require('json/categories.json');

const INITIAL_STATE = {
    selected: {},
    all: categories
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_CATEGORY:
            return { ...state, selected: action.payload };
        default:
            return state;
    }
}