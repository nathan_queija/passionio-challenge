import { combineReducers } from 'redux';
import verticals from './Verticals';
import categories from './Categories';
import courses from './Courses';

export default combineReducers({
    verticals,
    categories,
    courses
});