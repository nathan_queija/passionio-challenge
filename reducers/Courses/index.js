import { SET_COURSE } from 'actions';

const courses = require('json/courses.json');

const INITIAL_STATE = {
    selected: {},
    all: courses
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_COURSE:
            return { ...state, selected: action.payload };
        default:
            return state;
    }
}