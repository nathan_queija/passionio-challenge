import { SET_VERTICAL } from 'actions';

const verticals = require('json/verticals.json');

const INITIAL_STATE = {
    selected: {},
    all: verticals
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_VERTICAL:
            return { ...state, selected: action.payload };
        default:
            return state;
    }
}