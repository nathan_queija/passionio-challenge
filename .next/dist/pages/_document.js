'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _document = require('next/dist/server/document.js');

var _document2 = _interopRequireDefault(_document);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/Projetos/Testes/passion.io/pages/_document.js?entry';


var MyDocument = function (_Document) {
    (0, _inherits3.default)(MyDocument, _Document);

    function MyDocument() {
        (0, _classCallCheck3.default)(this, MyDocument);

        return (0, _possibleConstructorReturn3.default)(this, (MyDocument.__proto__ || (0, _getPrototypeOf2.default)(MyDocument)).apply(this, arguments));
    }

    (0, _createClass3.default)(MyDocument, [{
        key: 'render',
        value: function render() {
            return _react2.default.createElement('html', { lang: 'en', dir: 'ltr', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 7
                }
            }, _react2.default.createElement(_document.Head, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 8
                }
            }, _react2.default.createElement('title', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 9
                }
            }, 'Passion.io'), _react2.default.createElement('meta', { charSet: 'utf-8', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 10
                }
            }), _react2.default.createElement('meta', {
                name: 'viewport',
                content: 'user-scalable=0, initial-scale=1, ' + 'minimum-scale=1, width=device-width, height=device-height',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 12
                }
            }), _react2.default.createElement('link', {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 20
                }
            }), _react2.default.createElement('link', { href: 'https://fonts.googleapis.com/icon?family=Material+Icons',
                rel: 'stylesheet', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                }
            }), _react2.default.createElement('link', { rel: 'shortcut icon', type: 'image/x-icon', href: '/static/favicon.ico', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 26
                }
            })), _react2.default.createElement('body', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                }
            }, _react2.default.createElement(_document.Main, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 31
                }
            }), _react2.default.createElement(_document.NextScript, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 32
                }
            })));
        }
    }]);

    return MyDocument;
}(_document2.default);

exports.default = MyDocument;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL19kb2N1bWVudC5qcyJdLCJuYW1lcyI6WyJSZWFjdCIsIkRvY3VtZW50IiwiSGVhZCIsIk1haW4iLCJOZXh0U2NyaXB0IiwiTXlEb2N1bWVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxBQUFPOzs7O0FBQ1AsQUFBTyxBQUFZLEFBQU0sQUFBTTs7Ozs7Ozs7O0lBRXpCLEE7Ozs7Ozs7Ozs7O2lDQUNPLEFBQ0w7bUNBQ0ksY0FBQSxVQUFNLE1BQU4sQUFBVyxNQUFLLEtBQWhCLEFBQW9COzhCQUFwQjtnQ0FBQSxBQUNBO0FBREE7YUFBQSxrQkFDQSxBQUFDOzs4QkFBRDtnQ0FBQSxBQUNJO0FBREo7QUFBQSwrQkFDSSxjQUFBOzs4QkFBQTtnQ0FBQTtBQUFBO0FBQUEsZUFESixBQUNJLEFBQ0EsdURBQU0sU0FBTixBQUFjOzhCQUFkO2dDQUZKLEFBRUksQUFFQTtBQUZBOztzQkFFQSxBQUNTLEFBQ0w7eUJBQ0ksdUNBSFIsQUFJUTs7OEJBSlI7Z0NBSkosQUFJSSxBQVFBO0FBUkE7QUFDSTtxQkFPSixBQUNRLEFBQ0o7c0JBRkosQUFFUzs7OEJBRlQ7Z0NBWkosQUFZSSxBQUlBO0FBSkE7QUFDSSx3REFHRSxNQUFOLEFBQVcsQUFDTDtxQkFETixBQUNVOzhCQURWO2dDQWhCSixBQWdCSSxBQUVBO0FBRkE7d0RBRU0sS0FBTixBQUFVLGlCQUFnQixNQUExQixBQUErQixnQkFBZSxNQUE5QyxBQUFtRDs4QkFBbkQ7Z0NBbkJKLEFBQ0EsQUFrQkksQUFHSjtBQUhJO2lDQUdKLGNBQUE7OzhCQUFBO2dDQUFBLEFBRUE7QUFGQTtBQUFBLCtCQUVBLEFBQUM7OzhCQUFEO2dDQUZBLEFBRUEsQUFDQTtBQURBO0FBQUEsZ0NBQ0EsQUFBQzs7OEJBQUQ7Z0NBMUJKLEFBQ0ksQUFzQkEsQUFHQSxBQUlQO0FBSk87QUFBQTs7Ozs7QUE1QmEsQSxBQW1DekI7O2tCQUFBLEFBQWUiLCJmaWxlIjoiX2RvY3VtZW50LmpzP2VudHJ5Iiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYXRoYW5xdWVpamEvRGVza3RvcC9Qcm9qZXRvcy9UZXN0ZXMvcGFzc2lvbi5pbyJ9