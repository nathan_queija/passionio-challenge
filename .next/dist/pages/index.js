'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _style = require('styled-jsx/style.js');

var _style2 = _interopRequireDefault(_style);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _nextReduxWrapper = require('next-redux-wrapper');

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

var _store = require('../store');

var _store2 = _interopRequireDefault(_store);

var _Layout = require('../components/Layout');

var _Layout2 = _interopRequireDefault(_Layout);

var _SearchFilter = require('../components/SearchFilter');

var _SearchFilter2 = _interopRequireDefault(_SearchFilter);

var _Category = require('../components/Category');

var _Category2 = _interopRequireDefault(_Category);

var _categories = require('../actions/categories');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/Projetos/Testes/passion.io/pages/index.js?entry';


var Index = function (_React$Component) {
    (0, _inherits3.default)(Index, _React$Component);

    function Index(props) {
        (0, _classCallCheck3.default)(this, Index);

        return (0, _possibleConstructorReturn3.default)(this, (Index.__proto__ || (0, _getPrototypeOf2.default)(Index)).call(this, props));
    }

    (0, _createClass3.default)(Index, [{
        key: 'render',
        value: function render() {
            var _props = this.props,
                verticals = _props.verticals,
                categories = _props.categories,
                courses = _props.courses,
                setCategory = _props.setCategory,
                setCourse = _props.setCourse,
                setVertical = _props.setVertical;

            var categoriesFiltered = categories.all.filter(function (category) {
                return category.Verticals === verticals.selected.Id;
            });
            var coursesFiltered = courses.all.filter(function (course) {
                return course.Categories === categories.selected.Id;
            });

            return _react2.default.createElement(_Layout2.default, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                }
            }, _react2.default.createElement(_style2.default, {
                styleId: '1270758536',
                css: 'h1.jsx-1270758536,h2.jsx-1270758536{text-align:center;}h5.fancy.jsx-1270758536{width:80%;text-align:center;border-bottom:1px solid #000;line-height:0.1em;margin:auto;margin-top:40px;}h5.fancy.jsx-1270758536 span.jsx-1270758536{background:#fff;padding:0 10px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2luZGV4LmpzP2VudHJ5Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXlCbUIsQUFHMkMsQUFJYixBQVFxQixVQVBiLE1BTzRCLEVBWDdDLFVBSzZCLEdBTWlCLDBCQUw3QixrQkFDTCxZQUNJLGdCQUVoQiIsImZpbGUiOiJwYWdlcy9pbmRleC5qcz9lbnRyeSIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbmF0aGFucXVlaWphL0Rlc2t0b3AvUHJvamV0b3MvVGVzdGVzL3Bhc3Npb24uaW8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHdpdGhSZWR1eCBmcm9tICduZXh0LXJlZHV4LXdyYXBwZXInO1xuaW1wb3J0IGluaXRTdG9yZSBmcm9tICcuLi9zdG9yZSc7XG5pbXBvcnQgTGF5b3V0IGZyb20gJ2NvbXBvbmVudHMvTGF5b3V0JztcbmltcG9ydCBTZWFyY2hGaWx0ZXIgZnJvbSAnY29tcG9uZW50cy9TZWFyY2hGaWx0ZXInO1xuaW1wb3J0IENhdGVnb3J5IGZyb20gJ2NvbXBvbmVudHMvQ2F0ZWdvcnknO1xuaW1wb3J0IHsgc2V0Q2F0ZWdvcnksIHNldENvdXJzZSwgc2V0VmVydGljYWwgfSBmcm9tICdhY3Rpb25zL2NhdGVnb3JpZXMnO1xuXG5cblxuY2xhc3MgSW5kZXggZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuXG4gICAgY29uc3RydWN0b3IocHJvcHMpe1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgfVxuXG5cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGNvbnN0IHt2ZXJ0aWNhbHMsIGNhdGVnb3JpZXMsIGNvdXJzZXMsIHNldENhdGVnb3J5LCBzZXRDb3Vyc2UsIHNldFZlcnRpY2FsfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGNhdGVnb3JpZXNGaWx0ZXJlZCA9IGNhdGVnb3JpZXMuYWxsLmZpbHRlcihjYXRlZ29yeSA9PiBjYXRlZ29yeS5WZXJ0aWNhbHMgPT09IHZlcnRpY2Fscy5zZWxlY3RlZC5JZCk7XG4gICAgICAgIGNvbnN0IGNvdXJzZXNGaWx0ZXJlZCA9IGNvdXJzZXMuYWxsLmZpbHRlcihjb3Vyc2UgPT4gY291cnNlLkNhdGVnb3JpZXMgPT09IGNhdGVnb3JpZXMuc2VsZWN0ZWQuSWQpO1xuXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgPExheW91dD5cbiAgICAgICAgICAgICAgPHN0eWxlIGpzeD5cbiAgICAgICAgICAgICAgICAgIHtgXG4gICAgICAgICAgICAgICAgICAgIGgxLCBoMiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBoNS5mYW5jeSB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOjgwJTtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDAwO1xuICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDowLjFlbTtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA0MHB4O1xuXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaDUuZmFuY3kgc3BhbiB7IGJhY2tncm91bmQ6I2ZmZjsgcGFkZGluZzowIDEwcHg7IH1cbiAgICAgICAgICAgICAgICAgIGB9XG4gICAgICAgICAgICAgIDwvc3R5bGU+XG4gICAgICAgICAgICAgIDxoMT5Gcm9udGVuZCBDb2RpbmcgQ2hhbGxlbmdlPC9oMT5cbiAgICAgICAgICAgICAgPGgyPlBhc3Npb25hdGUgTmF2aWdhdGlvbjwvaDI+XG4gICAgICAgICAgICAgIDxTZWFyY2hGaWx0ZXIvPlxuICAgICAgICAgICAgICA8aDUgY2xhc3NOYW1lPVwiZmFuY3lcIj48c3Bhbj5vciBjbGljayBvbiB0aGUgdmVydGljYWxzIGJlbG93IHRvIG5hdmlnYXRlIDwvc3Bhbj48L2g1PlxuICAgICAgICAgICAgICA8Q2F0ZWdvcnkgbmFtZT1cIlZlcnRpY2Fsc1wiIG1lc3NhZ2U9XCJTZWxlY3QgYSBWZXJ0aWNhbDpcIiBjb250ZW50PXt2ZXJ0aWNhbHMuYWxsfSBvbkNob29zZT17c2V0VmVydGljYWx9IC8+XG4gICAgICAgICAgICAgIHt2ZXJ0aWNhbHMuc2VsZWN0ZWQuSWQgJiYgPENhdGVnb3J5IG5hbWU9XCJDYXRlZ29yaWVzXCIgbWVzc2FnZT1cIlNlbGVjdCBhIENhdGVnb3J5OlwiIGNvbnRlbnQ9e2NhdGVnb3JpZXNGaWx0ZXJlZH0gb25DaG9vc2U9e3NldENhdGVnb3J5fS8+fVxuICAgICAgICAgICAgICB7Y2F0ZWdvcmllcy5zZWxlY3RlZC5JZCAmJiA8Q2F0ZWdvcnkgbmFtZT1cIkNvdXJzZXNcIiAgY29udGVudD17Y291cnNlc0ZpbHRlcmVkfSBvbkNob29zZT17c2V0Q291cnNlfS8+fVxuXG4gICAgICAgICAgPC9MYXlvdXQ+XG4gICAgICAgICk7XG4gICAgfVxufVxuXG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IHZlcnRpY2FscywgY2F0ZWdvcmllcywgY291cnNlcyB9KSA9PiAoe1xuICAgICAgICB2ZXJ0aWNhbHMsXG4gICAgICAgIGNhdGVnb3JpZXMsXG4gICAgICAgIGNvdXJzZXNcbiAgICB9KTtcbmV4cG9ydCBkZWZhdWx0IHdpdGhSZWR1eChpbml0U3RvcmUsIG1hcFN0YXRlVG9Qcm9wcywge3NldENhdGVnb3J5LCBzZXRWZXJ0aWNhbCwgc2V0Q291cnNlfSkoSW5kZXgpO1xuIl19 */\n/*@ sourceURL=pages/index.js?entry */'
            }), _react2.default.createElement('h1', {
                className: 'jsx-1270758536',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 43
                }
            }, 'Frontend Coding Challenge'), _react2.default.createElement('h2', {
                className: 'jsx-1270758536',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 44
                }
            }, 'Passionate Navigation'), _react2.default.createElement(_SearchFilter2.default, {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 45
                }
            }), _react2.default.createElement('h5', {
                className: 'jsx-1270758536' + ' ' + 'fancy',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 46
                }
            }, _react2.default.createElement('span', {
                className: 'jsx-1270758536',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 46
                }
            }, 'or click on the verticals below to navigate ')), _react2.default.createElement(_Category2.default, { name: 'Verticals', message: 'Select a Vertical:', content: verticals.all, onChoose: setVertical, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 47
                }
            }), verticals.selected.Id && _react2.default.createElement(_Category2.default, { name: 'Categories', message: 'Select a Category:', content: categoriesFiltered, onChoose: setCategory, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 48
                }
            }), categories.selected.Id && _react2.default.createElement(_Category2.default, { name: 'Courses', content: coursesFiltered, onChoose: setCourse, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 49
                }
            }));
        }
    }]);

    return Index;
}(_react2.default.Component);

var mapStateToProps = function mapStateToProps(_ref) {
    var verticals = _ref.verticals,
        categories = _ref.categories,
        courses = _ref.courses;
    return {
        verticals: verticals,
        categories: categories,
        courses: courses
    };
};
exports.default = (0, _nextReduxWrapper2.default)(_store2.default, mapStateToProps, { setCategory: _categories.setCategory, setVertical: _categories.setVertical, setCourse: _categories.setCourse })(Index);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbIlJlYWN0Iiwid2l0aFJlZHV4IiwiaW5pdFN0b3JlIiwiTGF5b3V0IiwiU2VhcmNoRmlsdGVyIiwiQ2F0ZWdvcnkiLCJzZXRDYXRlZ29yeSIsInNldENvdXJzZSIsInNldFZlcnRpY2FsIiwiSW5kZXgiLCJwcm9wcyIsInZlcnRpY2FscyIsImNhdGVnb3JpZXMiLCJjb3Vyc2VzIiwiY2F0ZWdvcmllc0ZpbHRlcmVkIiwiYWxsIiwiZmlsdGVyIiwiY2F0ZWdvcnkiLCJWZXJ0aWNhbHMiLCJzZWxlY3RlZCIsIklkIiwiY291cnNlc0ZpbHRlcmVkIiwiY291cnNlIiwiQ2F0ZWdvcmllcyIsIkNvbXBvbmVudCIsIm1hcFN0YXRlVG9Qcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFPLEFBQWU7Ozs7QUFDdEIsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBQ1AsQUFBUyxBQUFhLEFBQVc7Ozs7Ozs7SUFJM0IsQTttQ0FFRjs7bUJBQUEsQUFBWSxPQUFNOzRDQUFBOzttSUFBQSxBQUNSLEFBQ1Q7Ozs7O2lDQUdRO3lCQUN5RSxLQUR6RSxBQUM4RTtnQkFEOUUsQUFDRSxtQkFERixBQUNFO2dCQURGLEFBQ2Esb0JBRGIsQUFDYTtnQkFEYixBQUN5QixpQkFEekIsQUFDeUI7Z0JBRHpCLEFBQ2tDLHFCQURsQyxBQUNrQztnQkFEbEMsQUFDK0MsbUJBRC9DLEFBQytDO2dCQUQvQyxBQUMwRCxxQkFEMUQsQUFDMEQsQUFDL0Q7O2dCQUFNLGdDQUFxQixBQUFXLElBQVgsQUFBZSxPQUFPLG9CQUFBO3VCQUFZLFNBQUEsQUFBUyxjQUFjLFVBQUEsQUFBVSxTQUE3QyxBQUFzRDtBQUF2RyxBQUEyQixBQUMzQixhQUQyQjtnQkFDckIsMEJBQWtCLEFBQVEsSUFBUixBQUFZLE9BQU8sa0JBQUE7dUJBQVUsT0FBQSxBQUFPLGVBQWUsV0FBQSxBQUFXLFNBQTNDLEFBQW9EO0FBQS9GLEFBQXdCLEFBRXhCLGFBRndCOzttQ0FHdEIsQUFBQzs7OEJBQUQ7Z0NBQUE7QUFBQTtBQUFBLGFBQUE7eUJBQUE7cUJBQUEsQUFtQkk7QUFuQkosZ0NBbUJJLGNBQUE7MkJBQUE7OzhCQUFBO2dDQUFBO0FBQUE7QUFBQSxlQW5CSixBQW1CSSxBQUNBLDhDQUFBLGNBQUE7MkJBQUE7OzhCQUFBO2dDQUFBO0FBQUE7QUFBQSxlQXBCSixBQW9CSSxBQUNBLDBDQUFBLEFBQUM7OzhCQUFEO2dDQXJCSixBQXFCSSxBQUNBO0FBREE7QUFBQSxnQ0FDQSxjQUFBO29EQUFBLEFBQWM7OzhCQUFkO2dDQUFBLEFBQXNCO0FBQXRCO0FBQUEsK0JBQXNCLGNBQUE7MkJBQUE7OzhCQUFBO2dDQUFBO0FBQUE7QUFBQSxlQXRCMUIsQUFzQkksQUFBc0IsQUFDdEIsa0VBQUEsQUFBQyxvQ0FBUyxNQUFWLEFBQWUsYUFBWSxTQUEzQixBQUFtQyxzQkFBcUIsU0FBUyxVQUFqRSxBQUEyRSxLQUFLLFVBQWhGLEFBQTBGOzhCQUExRjtnQ0F2QkosQUF1QkksQUFDQztBQUREOzBCQUNDLEFBQVUsU0FBVixBQUFtQixzQkFBTSxBQUFDLG9DQUFTLE1BQVYsQUFBZSxjQUFhLFNBQTVCLEFBQW9DLHNCQUFxQixTQUF6RCxBQUFrRSxvQkFBb0IsVUFBdEYsQUFBZ0c7OEJBQWhHO2dDQXhCOUIsQUF3QjhCLEFBQ3pCO0FBRHlCO2FBQUEsY0FDekIsQUFBVyxTQUFYLEFBQW9CLHNCQUFNLEFBQUMsb0NBQVMsTUFBVixBQUFlLFdBQVcsU0FBMUIsQUFBbUMsaUJBQWlCLFVBQXBELEFBQThEOzhCQUE5RDtnQ0ExQmpDLEFBQ0UsQUF5QitCLEFBSXBDO0FBSm9DO2FBQUE7Ozs7O0VBdENyQixnQixBQUFNOztBQThDMUIsSUFBTSxrQkFBa0IsU0FBbEIsQUFBa0Isc0JBQUE7UUFBQSxBQUFHLGlCQUFILEFBQUc7UUFBSCxBQUFjLGtCQUFkLEFBQWM7UUFBZCxBQUEwQixlQUExQixBQUEwQjs7bUJBQWUsQUFFekQ7b0JBRnlELEFBR3pEO2lCQUhnQixBQUF5QztBQUFBLEFBQ3pEO0FBRFIsQUFLQTtrQkFBZSxBQUFVLGlEQUFWLEFBQXFCLGlCQUFpQixFQUFBLEFBQUMsc0NBQUQsQUFBYyxzQ0FBcEQsQUFBc0MsQUFBMkIsb0NBQWhGLEFBQWUsQUFBNkUiLCJmaWxlIjoiaW5kZXguanM/ZW50cnkiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL1Byb2pldG9zL1Rlc3Rlcy9wYXNzaW9uLmlvIn0=