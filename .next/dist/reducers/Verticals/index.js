'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
    var action = arguments[1];

    switch (action.type) {
        case _actions.SET_VERTICAL:
            return (0, _extends3.default)({}, state, { selected: action.payload });
        default:
            return state;
    }
};

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _actions = require('../../actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var verticals = require('../../json/verticals.json');

var INITIAL_STATE = {
    selected: {},
    all: verticals
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZHVjZXJzL1ZlcnRpY2Fscy9pbmRleC5qcyJdLCJuYW1lcyI6WyJTRVRfVkVSVElDQUwiLCJ2ZXJ0aWNhbHMiLCJyZXF1aXJlIiwiSU5JVElBTF9TVEFURSIsInNlbGVjdGVkIiwiYWxsIiwic3RhdGUiLCJhY3Rpb24iLCJ0eXBlIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7O2tCQVNlLFlBQXlDO1FBQS9CLEFBQStCLDRFQUF2QixBQUF1QjtRQUFSLEFBQVEsbUJBQ3BEOztZQUFRLE9BQVIsQUFBZSxBQUNYO0FBQUEsQUFBSyxBQUNEOzhDQUFBLEFBQVksU0FBTyxVQUFVLE9BQTdCLEFBQW9DLEFBQ3hDO0FBQ0k7bUJBSlIsQUFJUSxBQUFPLEFBRWxCOzs7Ozs7OztBQWhCRCxBQUFTOzs7O0FBRVQsSUFBTSxZQUFBLEFBQVksUUFBbEI7O0FBRUEsSUFBTTtjQUFnQixBQUNSLEFBQ1Y7U0FGSixBQUFzQixBQUViLEFBR1Q7QUFMc0IsQUFDbEIiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL1Byb2pldG9zL1Rlc3Rlcy9wYXNzaW9uLmlvIn0=