'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _redux = require('redux');

var _Verticals = require('./Verticals');

var _Verticals2 = _interopRequireDefault(_Verticals);

var _Categories = require('./Categories');

var _Categories2 = _interopRequireDefault(_Categories);

var _Courses = require('./Courses');

var _Courses2 = _interopRequireDefault(_Courses);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _redux.combineReducers)({
    verticals: _Verticals2.default,
    categories: _Categories2.default,
    courses: _Courses2.default
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZHVjZXJzL2luZGV4LmpzIl0sIm5hbWVzIjpbImNvbWJpbmVSZWR1Y2VycyIsInZlcnRpY2FscyIsImNhdGVnb3JpZXMiLCJjb3Vyc2VzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxBQUFTOztBQUNULEFBQU8sQUFBZTs7OztBQUN0QixBQUFPLEFBQWdCOzs7O0FBQ3ZCLEFBQU8sQUFBYSxBQUVwQjs7Ozs7OztBQUErQixBQUUzQjtBQUYyQixBQUczQjtBQUhKLEFBQWUsQUFBZ0I7QUFBQSxBQUMzQixDQURXIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYXRoYW5xdWVpamEvRGVza3RvcC9Qcm9qZXRvcy9UZXN0ZXMvcGFzc2lvbi5pbyJ9