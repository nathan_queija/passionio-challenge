'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
    var action = arguments[1];

    switch (action.type) {
        case _actions.SET_CATEGORY:
            return (0, _extends3.default)({}, state, { selected: action.payload });
        default:
            return state;
    }
};

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _actions = require('../../actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var categories = require('../../json/categories.json');

var INITIAL_STATE = {
    selected: {},
    all: categories
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZHVjZXJzL0NhdGVnb3JpZXMvaW5kZXguanMiXSwibmFtZXMiOlsiU0VUX0NBVEVHT1JZIiwiY2F0ZWdvcmllcyIsInJlcXVpcmUiLCJJTklUSUFMX1NUQVRFIiwic2VsZWN0ZWQiLCJhbGwiLCJzdGF0ZSIsImFjdGlvbiIsInR5cGUiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7a0JBU2UsWUFBeUM7UUFBL0IsQUFBK0IsNEVBQXZCLEFBQXVCO1FBQVIsQUFBUSxtQkFDcEQ7O1lBQVEsT0FBUixBQUFlLEFBQ1g7QUFBQSxBQUFLLEFBQ0Q7OENBQUEsQUFBWSxTQUFPLFVBQVUsT0FBN0IsQUFBb0MsQUFDeEM7QUFDSTttQkFKUixBQUlRLEFBQU8sQUFFbEI7Ozs7Ozs7O0FBaEJELEFBQVM7Ozs7QUFFVCxJQUFNLGFBQUEsQUFBYSxRQUFuQjs7QUFFQSxJQUFNO2NBQWdCLEFBQ1IsQUFDVjtTQUZKLEFBQXNCLEFBRWIsQUFHVDtBQUxzQixBQUNsQiIsImZpbGUiOiJpbmRleC5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbmF0aGFucXVlaWphL0Rlc2t0b3AvUHJvamV0b3MvVGVzdGVzL3Bhc3Npb24uaW8ifQ==