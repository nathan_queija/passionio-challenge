'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setCategory = setCategory;
exports.setVertical = setVertical;
exports.setCourse = setCourse;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _ = require('./');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function setCategory(category) {
    var _this = this;

    return function () {
        var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(dispatch) {
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            dispatch({ type: _.SET_CATEGORY, payload: category });

                        case 1:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this);
        }));

        return function (_x) {
            return _ref.apply(this, arguments);
        };
    }();
}

function setVertical(vertical) {
    var _this2 = this;

    return function () {
        var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(dispatch) {
            return _regenerator2.default.wrap(function _callee2$(_context2) {
                while (1) {
                    switch (_context2.prev = _context2.next) {
                        case 0:
                            dispatch({ type: _.SET_VERTICAL, payload: vertical });
                            dispatch({ type: _.SET_CATEGORY, payload: {} });

                        case 2:
                        case 'end':
                            return _context2.stop();
                    }
                }
            }, _callee2, _this2);
        }));

        return function (_x2) {
            return _ref2.apply(this, arguments);
        };
    }();
}

function setCourse(course) {
    var _this3 = this;

    return function () {
        var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(dispatch) {
            return _regenerator2.default.wrap(function _callee3$(_context3) {
                while (1) {
                    switch (_context3.prev = _context3.next) {
                        case 0:
                            dispatch({ type: _.SET_COURSE, payload: course });

                        case 1:
                        case 'end':
                            return _context3.stop();
                    }
                }
            }, _callee3, _this3);
        }));

        return function (_x3) {
            return _ref3.apply(this, arguments);
        };
    }();
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGlvbnMvY2F0ZWdvcmllcy5qcyJdLCJuYW1lcyI6WyJTRVRfQ0FURUdPUlkiLCJTRVRfVkVSVElDQUwiLCJTRVRfQ09VUlNFIiwic2V0Q2F0ZWdvcnkiLCJjYXRlZ29yeSIsImRpc3BhdGNoIiwidHlwZSIsInBheWxvYWQiLCJzZXRWZXJ0aWNhbCIsInZlcnRpY2FsIiwic2V0Q291cnNlIiwiY291cnNlIl0sIm1hcHBpbmdzIjoiOzs7OztRQUdPLEFBQVM7UUFNVCxBQUFTO1FBT1QsQUFBUzs7Ozs7Ozs7OztBQWZoQixBQUFTLEFBQWMsQUFBYyxBQUFrQixBQUV2RDs7OztBQUFPLHFCQUFBLEFBQXFCLFVBQVU7Z0JBQ2xDOzt1QkFBQTs0RkFBTyxpQkFBQSxBQUFNLFVBQU47MEVBQUE7MEJBQUE7cURBQUE7NkJBQ0g7cUNBQVMsRUFBQSxBQUFFLEFBQU0sc0JBQWMsU0FENUIsQUFDSCxBQUFTLEFBQStCOzs2QkFEckM7NkJBQUE7NENBQUE7O0FBQUE7d0JBQUE7QUFBUDs7NkJBQUE7b0NBQUE7QUFBQTtBQUdIO0FBRUQ7O0FBQU8scUJBQUEsQUFBcUIsVUFBVTtpQkFDbEM7O3VCQUFBOzZGQUFPLGtCQUFBLEFBQU0sVUFBTjs0RUFBQTswQkFBQTt1REFBQTs2QkFDSDtxQ0FBUyxFQUFBLEFBQUUsQUFBTSxzQkFBYyxTQUEvQixBQUFTLEFBQStCLEFBQ3hDO3FDQUFTLEVBQUEsQUFBRSxBQUFNLHNCQUFjLFNBRjVCLEFBRUgsQUFBUyxBQUErQjs7NkJBRnJDOzZCQUFBOzZDQUFBOztBQUFBO3lCQUFBO0FBQVA7OzhCQUFBO3FDQUFBO0FBQUE7QUFJSDtBQUVEOztBQUFPLG1CQUFBLEFBQW1CLFFBQVE7aUJBQzlCOzt1QkFBQTs2RkFBTyxrQkFBQSxBQUFNLFVBQU47NEVBQUE7MEJBQUE7dURBQUE7NkJBQ0g7cUNBQVMsRUFBQSxBQUFFLEFBQU0sb0JBQVksU0FEMUIsQUFDSCxBQUFTLEFBQTZCOzs2QkFEbkM7NkJBQUE7NkNBQUE7O0FBQUE7eUJBQUE7QUFBUDs7OEJBQUE7cUNBQUE7QUFBQTtBQUdIIiwiZmlsZSI6ImNhdGVnb3JpZXMuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL1Byb2pldG9zL1Rlc3Rlcy9wYXNzaW9uLmlvIn0=