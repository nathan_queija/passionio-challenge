'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.concatContent = concatContent;
exports.findMatches = findMatches;

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function concatContent(arrays) {
    var finalArray = [];

    arrays.forEach(function (element) {
        var actualContentArray = element.content.map(function (actualContent) {
            return { name: element.name, content: actualContent };
        });
        finalArray.push.apply(finalArray, (0, _toConsumableArray3.default)(actualContentArray));
    });

    return finalArray;
}

function findMatches(word, array) {
    return array.filter(function (actual) {
        var regex = new RegExp(word, 'gi');
        return actual.content.Name.match(regex);
    });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWxzL2luZGV4LmpzIl0sIm5hbWVzIjpbImNvbmNhdENvbnRlbnQiLCJhcnJheXMiLCJmaW5hbEFycmF5IiwiZm9yRWFjaCIsImFjdHVhbENvbnRlbnRBcnJheSIsImVsZW1lbnQiLCJjb250ZW50IiwibWFwIiwibmFtZSIsImFjdHVhbENvbnRlbnQiLCJwdXNoIiwiZmluZE1hdGNoZXMiLCJ3b3JkIiwiYXJyYXkiLCJmaWx0ZXIiLCJyZWdleCIsIlJlZ0V4cCIsImFjdHVhbCIsIk5hbWUiLCJtYXRjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7UUFBTyxBQUFTO1FBWVQsQUFBUzs7QUFaaEI7Ozs7OztBQUFPLHVCQUFBLEFBQXVCLFFBQVEsQUFDbEM7UUFBTSxhQUFOLEFBQW1CLEFBRW5COztXQUFBLEFBQU8sUUFBUSxtQkFBVyxBQUN0QjtZQUFNLDZCQUFxQixBQUFRLFFBQVIsQUFBZ0IsSUFBSSx5QkFBQTttQkFBa0IsRUFBQyxNQUFNLFFBQVAsQUFBZSxNQUFNLFNBQXZDLEFBQWtCLEFBQThCO0FBQS9GLEFBQTJCLEFBQzNCLFNBRDJCO21CQUMzQixBQUFXLHdEQUFYLEFBQW1CLEFBQ3RCO0FBSEQsQUFLQTs7V0FBQSxBQUFPLEFBQ1Y7QUFHRDs7QUFBTyxxQkFBQSxBQUFxQixNQUFyQixBQUEyQixPQUFNLEFBQ3BDO2lCQUFPLEFBQU0sT0FBTyxrQkFBVSxBQUMxQjtZQUFNLFFBQVEsSUFBQSxBQUFJLE9BQUosQUFBVyxNQUF6QixBQUFjLEFBQWlCLEFBQy9CO2VBQU8sT0FBQSxBQUFPLFFBQVAsQUFBZSxLQUFmLEFBQW9CLE1BQTNCLEFBQU8sQUFBMEIsQUFDcEM7QUFIRCxBQUFPLEFBSVYsS0FKVSIsImZpbGUiOiJpbmRleC5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbmF0aGFucXVlaWphL0Rlc2t0b3AvUHJvamV0b3MvVGVzdGVzL3Bhc3Npb24uaW8ifQ==