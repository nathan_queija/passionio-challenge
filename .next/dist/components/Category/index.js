"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/nathanqueija/Desktop/Projetos/Testes/passion.io/components/Category/index.js";


var Category = function (_Component) {
    (0, _inherits3.default)(Category, _Component);

    function Category() {
        (0, _classCallCheck3.default)(this, Category);

        return (0, _possibleConstructorReturn3.default)(this, (Category.__proto__ || (0, _getPrototypeOf2.default)(Category)).apply(this, arguments));
    }

    (0, _createClass3.default)(Category, [{
        key: "getContent",
        value: function getContent(content) {
            var onChoose = this.props.onChoose;

            return content.map(function (actualContent) {
                return _react2.default.createElement("div", { key: actualContent.Id, className: "item", onClick: function onClick() {
                        return onChoose(actualContent);
                    }, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 10
                    }
                }, _react2.default.createElement("p", {
                    __source: {
                        fileName: _jsxFileName,
                        lineNumber: 11
                    }
                }, actualContent.Name), actualContent.Author && _react2.default.createElement("span", {
                    __source: {
                        fileName: _jsxFileName,
                        lineNumber: 12
                    }
                }, "by ", actualContent.Author));
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _props = this.props,
                message = _props.message,
                name = _props.name,
                content = _props.content;

            return _react2.default.createElement("div", {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 23
                }
            }, name && _react2.default.createElement("h2", { className: name.toLowerCase() + " category-title", __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                }
            }, name), message && _react2.default.createElement("p", {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 25
                }
            }, message), _react2.default.createElement("div", { className: "category-container", __source: {
                    fileName: _jsxFileName,
                    lineNumber: 26
                }
            }, this.getContent(content)));
        }
    }]);

    return Category;
}(_react.Component);

exports.default = Category;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvQ2F0ZWdvcnkvaW5kZXguanMiXSwibmFtZXMiOlsiUmVhY3QiLCJDb21wb25lbnQiLCJDYXRlZ29yeSIsImNvbnRlbnQiLCJvbkNob29zZSIsInByb3BzIiwibWFwIiwiYWN0dWFsQ29udGVudCIsIklkIiwiTmFtZSIsIkF1dGhvciIsIm1lc3NhZ2UiLCJuYW1lIiwidG9Mb3dlckNhc2UiLCJnZXRDb250ZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLEFBQU8sQUFBUzs7Ozs7Ozs7O0ksQUFFVjs7Ozs7Ozs7Ozs7bUNBR1MsQSxTQUFRO2dCQUFBLEFBQ1IsV0FBWSxLQURKLEFBQ1MsTUFEVCxBQUNSLEFBQ1I7OzJCQUFPLEFBQVEsSUFBSSx5QkFBaUIsQUFDaEM7dUNBQ0ksY0FBQSxTQUFLLEtBQUssY0FBVixBQUF3QixJQUFJLFdBQTVCLEFBQXNDLFFBQU8sU0FBUyxtQkFBQTsrQkFBTSxTQUFOLEFBQU0sQUFBUztBQUFyRTtrQ0FBQTtvQ0FBQSxBQUNJO0FBREo7aUJBQUEsa0JBQ0ksY0FBQTs7a0NBQUE7b0NBQUEsQUFBSTtBQUFKO0FBQUEsaUNBREosQUFDSSxBQUFrQixBQUNqQixxQkFBQSxBQUFjLDBCQUFVLGNBQUE7O2tDQUFBO29DQUFBO0FBQUE7QUFBQSxpQkFBQSxFQUFVLHFCQUgzQyxBQUNJLEFBRTZCLEFBQXdCLEFBRzVEO0FBUEQsQUFBTyxBQVFULGFBUlM7Ozs7aUNBV0Y7eUJBRTZCLEtBRjdCLEFBRWtDO2dCQUZsQyxBQUVHLGlCQUZILEFBRUc7Z0JBRkgsQUFFWSxjQUZaLEFBRVk7Z0JBRlosQUFFa0IsaUJBRmxCLEFBRWtCLEFBQ3RCOzttQ0FDSSxjQUFBOzs4QkFBQTtnQ0FBQSxBQUNLO0FBREw7QUFBQSxhQUFBLDBCQUNhLGNBQUEsUUFBSSxXQUFjLEtBQWQsQUFBYyxBQUFLLGdCQUF2Qjs4QkFBQTtnQ0FBQSxBQUF3RDtBQUF4RDthQUFBLEVBRGIsQUFDYSxBQUNSLGtDQUFXLGNBQUE7OzhCQUFBO2dDQUFBLEFBQUk7QUFBSjtBQUFBLGFBQUEsRUFGaEIsQUFFZ0IsQUFDWiwwQkFBQSxjQUFBLFNBQUssV0FBTCxBQUFlOzhCQUFmO2dDQUFBLEFBQ0s7QUFETDtvQkFDSyxBQUFLLFdBTGxCLEFBQ0ksQUFHSSxBQUNLLEFBQWdCLEFBSWhDOzs7OztBQTVCa0IsQSxBQStCdkI7O2tCQUFBLEFBQWUiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL1Byb2pldG9zL1Rlc3Rlcy9wYXNzaW9uLmlvIn0=