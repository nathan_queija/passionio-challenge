'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _style = require('styled-jsx/style.js');

var _style2 = _interopRequireDefault(_style);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _utils = require('../../utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/Projetos/Testes/passion.io/components/SearchFilter/index.js';


var SearchFilter = function (_React$Component) {
    (0, _inherits3.default)(SearchFilter, _React$Component);

    function SearchFilter(props) {
        (0, _classCallCheck3.default)(this, SearchFilter);

        var _this = (0, _possibleConstructorReturn3.default)(this, (SearchFilter.__proto__ || (0, _getPrototypeOf2.default)(SearchFilter)).call(this, props));

        var verticals = props.verticals,
            courses = props.courses,
            categories = props.categories;

        _this.state = {
            content: (0, _utils.concatContent)([{ name: "Vertical", content: verticals.all }, { name: "Course", content: courses.all }, { name: "Category", content: categories.all }]),
            matches: []
        };
        return _this;
    }

    (0, _createClass3.default)(SearchFilter, [{
        key: 'onChangeSearch',
        value: function onChangeSearch(event) {
            var value = event.target.value;

            if (value.length > 0) {
                var matches = (0, _utils.findMatches)(value, this.state.content);
                this.setState({ matches: matches });
            } else {
                this.setState({ matches: [] });
            }
        }
    }, {
        key: 'displayMatches',
        value: function displayMatches() {
            var matches = this.state.matches;

            return _react2.default.createElement('ul', { className: 'suggestions', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 37
                }
            }, matches.map(function (_ref) {
                var name = _ref.name,
                    content = _ref.content;

                return _react2.default.createElement('li', { key: name + content.Id, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 40
                    }
                }, _react2.default.createElement('span', { className: 'name', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 41
                    }
                }, '' + content.Name), _react2.default.createElement('span', { className: 'category ' + (name === 'Category' ? 'categories' : name === 'Vertical' ? 'verticals' : 'courses'), __source: {
                        fileName: _jsxFileName,
                        lineNumber: 42
                    }
                }, name));
            }));
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement('div', {
                className: 'jsx-3961827364',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 55
                }
            }, _react2.default.createElement(_style2.default, {
                styleId: '3961827364',
                css: '.field.jsx-3961827364{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;position:relative;margin:auto;width:70%;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;}.field.jsx-3961827364>input[type=text].jsx-3961827364{-webkit-flex:1;-ms-flex:1;flex:1;padding:0.6em;border:none;border-bottom:0.2em solid rgb(197,31,0);background-color:#191A20;color:white;font-weight:bold;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvU2VhcmNoRmlsdGVyL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXdEcUIsQUFHbUMsQUFRTixpQ0FDTyxjQUNELFlBQzZCLGVBVnhCLGtCQUNMLE9BVWEsS0FUaEIsVUFDUyxVQVNMLFlBQ0ssaUJBQ3BCLDhCQVZBIiwiZmlsZSI6ImNvbXBvbmVudHMvU2VhcmNoRmlsdGVyL2luZGV4LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYXRoYW5xdWVpamEvRGVza3RvcC9Qcm9qZXRvcy9UZXN0ZXMvcGFzc2lvbi5pbyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IHsgY29uY2F0Q29udGVudCwgZmluZE1hdGNoZXMgfSBmcm9tICd1dGlscyc7XG5cblxuY2xhc3MgU2VhcmNoRmlsdGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcblxuICAgIGNvbnN0cnVjdG9yKHByb3BzKXtcbiAgICAgICAgc3VwZXIocHJvcHMpO1xuXG4gICAgICAgIGNvbnN0IHt2ZXJ0aWNhbHMsIGNvdXJzZXMsIGNhdGVnb3JpZXN9ID0gcHJvcHM7XG5cbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIGNvbnRlbnQ6IGNvbmNhdENvbnRlbnQoW1xuICAgICAgICAgICAgICAgIHtuYW1lOiBcIlZlcnRpY2FsXCIsIGNvbnRlbnQ6IHZlcnRpY2Fscy5hbGx9LFxuICAgICAgICAgICAgICAgIHtuYW1lOiBcIkNvdXJzZVwiLCBjb250ZW50OiBjb3Vyc2VzLmFsbH0sXG4gICAgICAgICAgICAgICAge25hbWU6IFwiQ2F0ZWdvcnlcIiwgY29udGVudDogY2F0ZWdvcmllcy5hbGx9XG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIG1hdGNoZXM6IFtdXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbkNoYW5nZVNlYXJjaChldmVudCl7XG4gICAgICAgIGNvbnN0IHt2YWx1ZX0gPSBldmVudC50YXJnZXQ7XG5cbiAgICAgICAgaWYodmFsdWUubGVuZ3RoID4gMCl7XG4gICAgICAgICAgICBjb25zdCBtYXRjaGVzID0gZmluZE1hdGNoZXModmFsdWUsIHRoaXMuc3RhdGUuY29udGVudClcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe21hdGNoZXN9KTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHttYXRjaGVzOiBbXX0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZGlzcGxheU1hdGNoZXMoKXtcbiAgICAgICAgY29uc3QgeyBtYXRjaGVzIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICByZXR1cm4oXG4gICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwic3VnZ2VzdGlvbnNcIj5cbiAgICAgICAgICAgICAgICB7bWF0Y2hlcy5tYXAoKHtuYW1lLCBjb250ZW50fSkgPT4gIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuKFxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGtleT17bmFtZStjb250ZW50LklkfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJuYW1lXCI+e2Ake2NvbnRlbnQuTmFtZX1gfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2BjYXRlZ29yeSAke25hbWUgPT09ICdDYXRlZ29yeScgPyAnY2F0ZWdvcmllcycgOiAoIG5hbWUgPT09ICdWZXJ0aWNhbCcgPyAndmVydGljYWxzJyA6ICdjb3Vyc2VzJyl9YH0+e25hbWV9PC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICA8L3VsPlxuICAgICAgICApXG4gICAgfVxuXG5cblxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgPHN0eWxlIGpzeD5cbiAgICAgICAgICAgICAgICAgICAge2BcbiAgICAgICAgICAgICAgICAgICAgLmZpZWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OmZsZXg7XG4gICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOjcwJTtcbiAgICAgICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjpyb3c7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAuZmllbGQ+aW5wdXRbdHlwZT10ZXh0XSB7XG4gICAgICAgICAgICAgICAgICAgICAgZmxleDoxO1xuICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6MC42ZW07XG4gICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206MC4yZW0gc29saWQgcmdiKDE5NywgMzEsIDApO1xuICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxOTFBMjA7XG4gICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgIDwvc3R5bGU+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaWVsZFwiIGlkPVwic2VhcmNoZm9ybVwiPlxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cInNlYXJjaHRlcm1cIiBwbGFjZWhvbGRlcj1cIlN0YXJ0IHR5cGluZyB0byBzZWFyY2ggdmVydGljYWxzLCBjYXRlZ29yaWVzIG9yIGNvdXJzZXMuLi5cIiBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZVNlYXJjaC5iaW5kKHRoaXMpfSAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIHt0aGlzLmRpc3BsYXlNYXRjaGVzKCl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKTtcbiAgICB9XG59XG5cblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgdmVydGljYWxzLCBjYXRlZ29yaWVzLCBjb3Vyc2VzIH0pID0+ICh7XG4gICAgdmVydGljYWxzLFxuICAgIGNhdGVnb3JpZXMsXG4gICAgY291cnNlc1xufSk7XG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbnVsbCkoU2VhcmNoRmlsdGVyKTtcbiJdfQ== */\n/*@ sourceURL=components/SearchFilter/index.js */'
            }), _react2.default.createElement('div', { id: 'searchform', className: 'jsx-3961827364' + ' ' + 'field',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 77
                }
            }, _react2.default.createElement('input', { type: 'text', id: 'searchterm', placeholder: 'Start typing to search verticals, categories or courses...', onChange: this.onChangeSearch.bind(this), className: 'jsx-3961827364',
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 78
                }
            })), this.displayMatches());
        }
    }]);

    return SearchFilter;
}(_react2.default.Component);

var mapStateToProps = function mapStateToProps(_ref2) {
    var verticals = _ref2.verticals,
        categories = _ref2.categories,
        courses = _ref2.courses;
    return {
        verticals: verticals,
        categories: categories,
        courses: courses
    };
};
exports.default = (0, _reactRedux.connect)(mapStateToProps, null)(SearchFilter);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvU2VhcmNoRmlsdGVyL2luZGV4LmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwiY29ubmVjdCIsImNvbmNhdENvbnRlbnQiLCJmaW5kTWF0Y2hlcyIsIlNlYXJjaEZpbHRlciIsInByb3BzIiwidmVydGljYWxzIiwiY291cnNlcyIsImNhdGVnb3JpZXMiLCJzdGF0ZSIsImNvbnRlbnQiLCJuYW1lIiwiYWxsIiwibWF0Y2hlcyIsImV2ZW50IiwidmFsdWUiLCJ0YXJnZXQiLCJsZW5ndGgiLCJzZXRTdGF0ZSIsIm1hcCIsIklkIiwiTmFtZSIsIm9uQ2hhbmdlU2VhcmNoIiwiYmluZCIsImRpc3BsYXlNYXRjaGVzIiwiQ29tcG9uZW50IiwibWFwU3RhdGVUb1Byb3BzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxBQUFPOzs7O0FBQ1AsQUFBUzs7QUFDVCxBQUFTLEFBQWU7Ozs7Ozs7SUFHbEIsQTswQ0FFRjs7MEJBQUEsQUFBWSxPQUFNOzRDQUFBOztzSkFBQSxBQUNSOztZQURRLEFBR1AsWUFITyxBQUcyQixNQUgzQixBQUdQO1lBSE8sQUFHSSxVQUhKLEFBRzJCLE1BSDNCLEFBR0k7WUFISixBQUdhLGFBSGIsQUFHMkIsTUFIM0IsQUFHYSxBQUUzQjs7Y0FBQSxBQUFLO3FCQUNRLDBCQUFjLENBQ25CLEVBQUMsTUFBRCxBQUFPLFlBQVksU0FBUyxVQURULEFBQ25CLEFBQXNDLE9BQ3RDLEVBQUMsTUFBRCxBQUFPLFVBQVUsU0FBUyxRQUZQLEFBRW5CLEFBQWtDLE9BQ2xDLEVBQUMsTUFBRCxBQUFPLFlBQVksU0FBUyxXQUp2QixBQUNBLEFBQWMsQUFHbkIsQUFBdUMsQUFFM0M7cUJBWFUsQUFLZCxBQUFhLEFBTUE7QUFOQSxBQUNUO2VBT1A7Ozs7O3VDLEFBRWMsT0FBTTtnQkFBQSxBQUNWLFFBQVMsTUFEQyxBQUNLLE9BREwsQUFDVixBQUVQOztnQkFBRyxNQUFBLEFBQU0sU0FBVCxBQUFrQixHQUFFLEFBQ2hCO29CQUFNLFVBQVUsd0JBQUEsQUFBWSxPQUFPLEtBQUEsQUFBSyxNQUF4QyxBQUFnQixBQUE4QixBQUM5QztxQkFBQSxBQUFLLFNBQVMsRUFBQyxTQUFmLEFBQWMsQUFDakI7QUFIRCxtQkFHSyxBQUNEO3FCQUFBLEFBQUssU0FBUyxFQUFDLFNBQWYsQUFBYyxBQUFVLEFBQzNCO0FBQ0o7Ozs7eUNBRWU7Z0JBQUEsQUFDSixVQUFZLEtBRFIsQUFDYSxNQURiLEFBQ0osQUFDUjs7bUNBQ0ksY0FBQSxRQUFJLFdBQUosQUFBYzs4QkFBZDtnQ0FBQSxBQUNLO0FBREw7YUFBQSxVQUNLLEFBQVEsSUFBSSxnQkFBc0I7b0JBQXBCLEFBQW9CLFlBQXBCLEFBQW9CO29CQUFkLEFBQWMsZUFBZCxBQUFjLEFBQy9COzt1Q0FDSSxjQUFBLFFBQUksS0FBSyxPQUFLLFFBQWQsQUFBc0I7a0NBQXRCO29DQUFBLEFBQ0k7QUFESjtpQkFBQSxrQkFDSSxjQUFBLFVBQU0sV0FBTixBQUFnQjtrQ0FBaEI7b0NBQUE7QUFBQTt3QkFBMkIsUUFEL0IsQUFDSSxBQUFtQyxBQUNuQyx1QkFBQSxjQUFBLFVBQU0sMEJBQXVCLFNBQUEsQUFBUyxhQUFULEFBQXNCLGVBQWlCLFNBQUEsQUFBUyxhQUFULEFBQXNCLGNBQTFGLEFBQU0sQUFBa0c7a0NBQXhHO29DQUFBLEFBQXVIO0FBQXZIO21CQUhSLEFBQ0ksQUFFSSxBQUlYO0FBVlQsQUFDSSxBQUNLLEFBV1o7Ozs7aUNBSVEsQUFDTDttQ0FDSSxjQUFBOzJCQUFBOzs4QkFBQTtnQ0FBQTtBQUFBO0FBQUEsYUFBQTt5QkFBQTtxQkFBQSxBQXNCSTtBQXRCSixnQ0FzQkksY0FBQSxTQUF1QixJQUF2QixBQUEwQixrREFBMUIsQUFBZTs7OEJBQWY7Z0NBQUEsQUFDSTtBQURKO3dEQUNXLE1BQVAsQUFBWSxRQUFPLElBQW5CLEFBQXNCLGNBQWEsYUFBbkMsQUFBK0MsOERBQTZELFVBQVUsS0FBQSxBQUFLLGVBQUwsQUFBb0IsS0FBMUksQUFBc0gsQUFBeUIsa0JBQS9JOzs4QkFBQTtnQ0F2QlIsQUFzQkksQUFDSSxBQUVIO0FBRkc7c0JBeEJaLEFBQ0ksQUF5QkssQUFBSyxBQUdqQjs7Ozs7RUE3RXNCLGdCQUFNLEE7O0FBaUZqQyxJQUFNLGtCQUFrQixTQUFsQixBQUFrQix1QkFBQTtRQUFBLEFBQUcsa0JBQUgsQUFBRztRQUFILEFBQWMsbUJBQWQsQUFBYztRQUFkLEFBQTBCLGdCQUExQixBQUEwQjs7bUJBQWUsQUFFN0Q7b0JBRjZELEFBRzdEO2lCQUhvQixBQUF5QztBQUFBLEFBQzdEO0FBREosQUFLQTtrQkFBZSx5QkFBQSxBQUFRLGlCQUFSLEFBQXlCLE1BQXhDLEFBQWUsQUFBK0IiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL1Byb2pldG9zL1Rlc3Rlcy9wYXNzaW9uLmlvIn0=